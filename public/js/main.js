(function($){
			var posts = [];
			var comments = {};
			var $posts = $('#posts');

			$('#posts').on('click','[data-readmore]',function(e){
				e.preventDefault();
				var id = $(this).data('readmore');
				var postComments = posts[id].comments;
				$('#posts').hide();
				if(postComments){
					$comments = '';
					postComments.forEach(function(comment){
						comment.image = '//via.placeholder.com/64x64/18bc9c/2c3e50';
						$comments += generateCommentTemplate(comment);
					});
					$('#comments').find('.panel-body').html($comments);
				}
				$('#comments').fadeIn('slow');
			});
			$('#back').on('click',function(e){
				e.preventDefault();
				$('#posts').fadeIn('slow');
				$('#comments').hide();
			});

			$.get(api('comments'),function(response){
				response.forEach(function(comment){
					if(typeof comments[comment.postId] == "undefined"){
						comments[comment.postId] = [];
					}
					comments[comment.postId].push(comment);
				});

				$.get(api('posts'),function(response){
					response.forEach(function(post){
						post.comments = comments[post.id];
						posts.push(post);
					});
					$(window).trigger('data:loaded');
				});
			});

			$(window).on('data:loaded',function(e){
				var i = 0;
				var limit = 10;

				var post = setInterval(function(){
					if(limit < i){
						clearInterval(post);
					}
					var data = posts[i];
					data.image = api('150/150/?'+(+new Date()),'image');
					$posts.append($(generatePostTemplate(data)));
					i++;
				},250);
			});
			function api(dir,type){
				type = type || 'data';
				var ENDPOINT = type=='data' ? '//jsonplaceholder.typicode.com' : '//lorempixel.com';
				var api = ENDPOINT+'/'+dir;
				return api;
			}
			function generatePostTemplate(data){
				var template = '<div class="media">'+
						  '<div class="media-left media-middle">'+
						    '<a href="#" data-readmore='+data.id+'>'+
						      '<img class="thumbnail media-object" src="'+data.image+'" alt="'+data.title+'">'+
						    '</a>'+
						  '</div>'+
						  '<div class="media-body">'+
						    '<h4 class="media-heading">'+data.title+'</h4>'+
						    '<p>'+data.body+'</p>'+
						    '<button class="btn btn-primary btn-sm" data-readmore='+data.id+'>Read more</button>'+
						  '</div>'+
						'</div>';
				return template;
			}
			function generateCommentTemplate(data){
				return '<div class="media">'+
						  '<div class="media-left">'+
						    '<a href="#">'+
						      '<img class="media-object img-circle" src="'+data.image+'" alt="'+data.image+'">'+
						    '</a>'+
						  '</div>'+
						  '<div class="media-body">'+
						    '<h4 class="media-heading">'+data.name+'</h4>'+
						    '<sup>'+data.email+'</sup>'+
						    '<p>'+data.body+'</p>'+
						  '</div>'+
						'</div>';
			}
		})(jQuery);